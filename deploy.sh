#!/usr/bin/env bash
set -euo pipefail
ssh dev.toolforge.org 'set -euo pipefail
echo Building...
become dewiki-votecheck toolforge build start https://gitlab.wikimedia.org/countcount/dewiki-votecheck.git
echo Deploying...
become dewiki-votecheck webservice restart
echo Done.'
