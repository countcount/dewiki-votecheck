/*
Copyright (C) 2023 Count Count <countvoncount123456@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
TODO:
* BSV support
* summary at the top
* Show timestamp for rights verification.
* Allow signature which only contain a Spezial:Beiträge-Link.
* multiple users in one vote -> check link(s) before timestamp, warn otherwise
* Handle renamed users somehow.
* Confirm that contribution for timestamp is from the same user.
* Check multiple users in a SQL query if faster.
* Only German errors.
* CI + tests
*/

#![deny(
    clippy::all,
    clippy::unnecessary_unwrap,
    clippy::pedantic,
    clippy::nursery
)]
#![allow(clippy::redundant_closure_for_method_calls)]

use std::collections::HashMap;
use std::fs::OpenOptions;
use std::net::SocketAddr;
use std::sync::Arc;
use std::time::Duration;

use anyhow::{Context, Result, bail};
use askama::Template;
use axum::extract::{Query, State};
use axum::http::StatusCode;
use axum::response::{Html, IntoResponse, Response};
use axum::{Router, routing};
use mwbot::Bot;
use mwbot::parsoid::node::WikiLink;
use mwbot::parsoid::prelude::{Heading, Section, Wikinode};
use mwbot::parsoid::{Wikicode, WikinodeIterator};
use once_cell_regex::regex;
use reqwest::Url;
use serde::Deserialize;
use tokio::sync::{Mutex, Semaphore};
use tokio::task::JoinSet;
use tokio::time::sleep;
use tower_http::trace::TraceLayer;
use tracing_subscriber::prelude::*;

enum UserOrError {
    UserUnchecked(String),
    UserPermitted {
        user: String,
        url: Url,
    },
    UserNotPermitted {
        user: String,
        url: Url,
    },
    Duplicate {
        user: String,
        other_section: String,
        other_no: usize,
    },
    NoUserLink,
    MultipleUserLinks(String, String),
    DotDotDot,
    Empty,
}

fn link_to_user(link: &WikiLink, bot: &Bot) -> Option<String> {
    let target = link.target();
    if let Some(captures) =
        regex!(r"^(?i)^(Spezial|Special):(Beiträge|Contributions)/(?<user>[^/]+)$")
            .captures(&target)
    {
        return Some(
            captures
                .name("user")
                .expect("regex matched so capture group exsits")
                .as_str()
                .trim()
                .replace('_', " "),
        );
    }
    if regex!(r"^(?i)^(Spezial|Special):").is_match(&target) {
        return None;
    }
    let page = bot.page(&target);
    let Ok(page) = page else { return None };
    if (page.namespace() != 2 && page.namespace() != 3) || target.contains('/') {
        return None;
    }
    Some(page.as_title().dbkey().replace('_', " "))
}

fn process_vote_li(
    li: &Wikinode,
    bot: &Bot,
    already_voted: &mut HashMap<String, (usize, usize)>,
    section_votes: &[(String, Vec<UserOrError>)],
    no: usize,
) -> UserOrError {
    let mut user = None::<String>;
    for elem in li
        .children()
        .filter(|e| e.as_element().filter(|e| &*e.name.local != "dl").is_some())
    {
        for link in elem.filter_links() {
            let Some(new_user) = link_to_user(&link, bot) else {
                continue;
            };
            match user {
                Some(ref old_user) if old_user == &new_user => {}
                Some(conflicting_user) => {
                    return UserOrError::MultipleUserLinks(conflicting_user, new_user);
                }
                None => {
                    user = Some(new_user);
                }
            }
        }
    }
    user.map_or_else(
        || {
            let text_contents = li.text_contents();
            let trimmed = text_contents.trim();
            match trimmed {
                "..." | "…" => UserOrError::DotDotDot,
                "" => UserOrError::Empty,
                _ => UserOrError::NoUserLink,
            }
        },
        |user| {
            if let Some((section, other_no)) = already_voted.get(&user) {
                UserOrError::Duplicate {
                    user,
                    other_section: section_votes[*section].0.clone(),
                    other_no: *other_no,
                }
            } else {
                already_voted.insert(user.clone(), (section_votes.len() - 1, no));
                UserOrError::UserUnchecked(user)
            }
        },
    )
}

fn get_voting_sections(
    section: &Section,
    bot: &Bot,
    heading_filter: &dyn Fn(&Heading) -> bool,
) -> Vec<(String, Vec<UserOrError>)> {
    let mut already_voted = HashMap::new();
    let mut section_votes = Vec::new();

    for section in section.iter_sections() {
        let Some(heading) = section.heading() else {
            continue;
        };
        if !heading_filter(&heading) {
            continue;
        }

        let mut votes = Vec::new();

        // FIXME: Should only iterate over direct children
        // of the top-level ordered list
        section_votes.push((heading.text_contents(), Vec::new()));
        for (no, li) in section
            .select("li")
            .iter()
            .enumerate()
            .map(|(i, li)| (i + 1, li))
        {
            let value = process_vote_li(li, bot, &mut already_voted, &section_votes, no);
            votes.push(value);
        }
        section_votes.last_mut().unwrap().1 = votes;
    }

    section_votes
}

fn get_check_url(html: &Wikicode) -> Result<String> {
    let mut check_url = None;
    for section in html.iter_sections() {
        if section
            .heading()
            .is_some_and(|h| h.level() == 2 && h.text_contents() == "Initiatoren und Unterstützer")
        {
            // ignore Unterstützer section
            continue;
        }
        for el in section.filter_external_links().iter().filter(|l| {
            let regex = regex!(
                "^https://(stimmberechtigung.toolforge.org/|iw.toolforge.org/stimmberechtigung|\
                tools.wmflabs.org/stimmberechtigung/)"
            );
            regex.is_match(l.target().as_str()) && !l.target().contains("user")
        }) {
            if let Some(other_url) = check_url {
                bail!(
                    "Multiple check URLs found: {} and {}",
                    other_url,
                    el.target()
                )
            }

            // resolve redirects here
            let regex =
                regex!(r"https://(iw\.toolforge\.org|tools\.wmflabs\.org)/stimmberechtigung/?");
            check_url = Some(
                regex
                    .replace(
                        el.target().as_str(),
                        "https://stimmberechtigung.toolforge.org/",
                    )
                    .into_owned(),
            );
        }
    }
    let Some(check_url) = check_url else {
        bail!("No check URL found!");
    };
    Ok(check_url)
}

fn get_user_check_url(check_url: &str, user: &str, bot: bool) -> Result<Url> {
    let mut url = Url::parse(check_url)?;
    {
        let mut query_pairs = url.query_pairs_mut();
        query_pairs.append_pair("user", user);
        if bot {
            query_pairs.append_pair("mode", "bot");
        }
    }
    Ok(url)
}

async fn vote_permitted(
    check_url: &str,
    user: &str,
    sg: bool,
    AppState {
        client,
        query_cache,
        check_sem,
    }: &AppState,
) -> Result<bool> {
    let url = get_user_check_url(check_url, user, true)?.to_string();
    {
        if let Some((cached_val, cached_val_sg)) = { query_cache.lock().await.get(&url).copied() } {
            return Ok(if sg { cached_val_sg } else { cached_val });
        }
    }

    let guard = check_sem.acquire().await?;
    let mut rep_count = 0;
    let res_text = loop {
        let request = client
            .get(get_user_check_url(check_url, user, true)?)
            .build()?;
        let res = client.execute(request).await?.error_for_status();
        match res {
            Ok(rp) => break rp.text().await?,
            Err(e)
                if e.status()
                    .is_some_and(|s| s == reqwest::StatusCode::TOO_MANY_REQUESTS)
                    && rep_count < 10 =>
            {
                sleep(Duration::from_secs(1)).await;
                rep_count += 1;
                continue;
            }
            Err(err) => {
                eprintln!("req err: {err}");
                return Err(anyhow::Error::from(err))
                    .context("Unexpected error calling sb service");
            }
        };
    };
    drop(guard);

    let val = res_text.contains("Allgemeine Stimmberechtigung: Ja");
    let sg_val = res_text.contains("Stimmberechtigung Schiedsgericht: Ja");
    query_cache.lock().await.insert(url, (val, sg_val));
    Ok(if sg { sg_val } else { val })
}

async fn check_voting_sections(
    voting_sections: &mut Vec<(String, Vec<UserOrError>)>,
    check_url: &str,
    sg: bool,
    state: &AppState,
) -> Result<()> {
    // prefill cache using parallel retrieval
    let mut joinset = JoinSet::new();
    for (_, votes) in &*voting_sections {
        for vote in votes {
            let UserOrError::UserUnchecked(user) = vote else {
                continue;
            };
            if state.query_cache.lock().await.contains_key(user) {
                continue;
            }
            let user = user.clone();
            let check_url = check_url.to_string();
            let state = state.clone();
            joinset.spawn(async move { vote_permitted(&check_url, &user, sg, &state).await });
        }
    }
    while let Some(res) = joinset.join_next().await {
        res??;
    }

    for (_, votes) in voting_sections {
        for vote in votes {
            let UserOrError::UserUnchecked(user) = vote else {
                continue;
            };
            if vote_permitted(check_url, user, sg, state).await? {
                *vote = UserOrError::UserPermitted {
                    user: user.clone(),
                    url: get_user_check_url(check_url, user, false)?,
                };
            } else {
                *vote = UserOrError::UserNotPermitted {
                    user: user.clone(),
                    url: get_user_check_url(check_url, user, false)?,
                };
            }
        }
    }

    Ok(())
}

async fn create_bot() -> Result<Bot> {
    let bot = Bot::builder(
        "https://de.wikipedia.org/w/api.php".to_string(),
        "https://de.wikipedia.org/api/rest_v1".to_string(),
    )
    .build()
    .await?;
    Ok(bot)
}

async fn get_page_contents(bot: &Bot, page_title: &str) -> Result<Wikicode> {
    let page = bot.page(page_title)?;
    let html = page.html().await?.into_mutable();
    Ok(html)
}

fn get_ak_voting_sections(html: &Wikicode, bot: &Bot) -> Result<Vec<(String, SectionAndVotes)>> {
    for s in html.iter_sections() {
        let Some(heading) = s.heading() else { continue };
        if heading.level() != 3 {
            continue;
        }

        return Ok(vec![(
            heading.text_contents(),
            get_voting_sections(&s, bot, &|h| {
                if h.level() != 4 {
                    return false;
                }
                let heading_text = h.text_contents();
                !regex!(r"(?i)^(Kommentare |Abstimmung)").is_match(&heading_text)
            }),
        )]);
    }
    bail!("Top-level section not found.");
}

type SectionAndVotes = Vec<(String, Vec<UserOrError>)>;

fn get_mb_voting_sections(html: &Wikicode, bot: &Bot) -> Vec<(String, SectionAndVotes)> {
    let mut res = Vec::new();
    for s in html.iter_sections() {
        let Some(heading) = s.heading() else { continue };
        if !(heading.level() == 2 && heading.text_contents() == "Abstimmung") {
            continue;
        }
        let main_section = s;
        let mut sections = Vec::new();
        let mut prev_section: Option<Section> = None;
        let mut prev_section_had_subsections = false;
        for s in main_section.iter_sections().into_iter().rev() {
            let Some(heading) = s.heading() else { continue };
            match prev_section {
                None => {}
                Some(prev_section) => {
                    match heading
                        .level()
                        .cmp(&prev_section.heading().unwrap().level())
                    {
                        std::cmp::Ordering::Less => {
                            // going up
                            if !prev_section_had_subsections {
                                sections.push(s.clone());
                            }
                            prev_section_had_subsections = true;
                        }
                        std::cmp::Ordering::Equal => {}
                        std::cmp::Ordering::Greater => {
                            // going down
                            prev_section_had_subsections = false;
                        }
                    }
                }
            }
            prev_section = Some(s);
        }
        sections.reverse();
        for s in sections {
            let Some(heading) = s.heading() else { continue };
            res.push((
                heading.text_contents(),
                get_voting_sections(&s, bot, &|h| h.level() != heading.level()),
            ));
        }
        break;
    }
    res
}

async fn check_votes_1(
    check_url: &str,
    page_title: &str,
    mut voting_sections: Vec<(String, SectionAndVotes)>,
    state: &AppState,
) -> Result<String> {
    let sg = page_title.to_ascii_lowercase().contains("schiedsgericht");
    for (_, voting_sections) in &mut voting_sections {
        check_voting_sections(voting_sections, check_url, sg, state).await?;
    }
    Ok(CheckTemplate {
        page: page_title,
        sg,
        voting_sections,
    }
    .render()?)
}

async fn check_votes(page_title: &str, state: &AppState) -> Result<Html<String>> {
    let res = if regex!(
        "(?i)^(Wikipedia:((Admin|Oversight|Bürokraten)kandidaturen|\
        (Checkuser|Schiedsgericht)/Wahl)|Benutzer:Count Count/votecheck/test)/.+",
    )
    .is_match(page_title)
    {
        let (check_url, voting_sections) = {
            let bot = create_bot().await?;
            let html = get_page_contents(&bot, page_title).await?;
            (get_check_url(&html)?, get_ak_voting_sections(&html, &bot)?)
        };
        check_votes_1(&check_url, page_title, voting_sections, state).await?
    } else if regex!(r"(?i)^Wikipedia:Meinungsbilder/[^/]+").is_match(page_title) {
        let (check_url, voting_sections) = {
            let bot = create_bot().await?;
            let html = get_page_contents(&bot, page_title).await?;
            (get_check_url(&html)?, get_mb_voting_sections(&html, &bot))
        };
        check_votes_1(&check_url, page_title, voting_sections, state).await?
    } else {
        bail!("Die Prüfung funktioniert nur für Wahlen und Meinungsbilder.");
    };

    Ok(Html(res))
}

#[derive(Deserialize)]
struct PageParams {
    page: String,
}

async fn check_page(
    params: Query<PageParams>,
    State(state): State<AppState>,
) -> Result<Html<String>, AppError> {
    let page = params.page.trim().replace('_', " ");
    Ok(check_votes(&page, &state).await?)
}

#[derive(Template)]
#[template(path = "check.html")]
struct CheckTemplate<'a> {
    page: &'a str,
    sg: bool,
    voting_sections: Vec<(String, SectionAndVotes)>,
}

fn init_logging() -> Result<()> {
    let file_writer = OpenOptions::new()
        .create(true)
        .append(true)
        .open("log.txt")?;

    let file_output_layer = tracing_subscriber::fmt::layer().with_writer(file_writer);

    let filter = tracing_subscriber::filter::Targets::new().with_default(tracing::Level::INFO);

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(file_output_layer)
        .with(filter)
        .init();

    Ok(())
}

#[derive(Clone)]
struct AppState {
    client: reqwest::Client,
    query_cache: Arc<Mutex<HashMap<String, (bool, bool)>>>,
    check_sem: Arc<Semaphore>,
}

struct AppError(anyhow::Error);

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        (StatusCode::INTERNAL_SERVER_ERROR, format!("{}", self.0)).into_response()
    }
}

impl<E> From<E> for AppError
where
    E: Into<anyhow::Error>,
{
    fn from(err: E) -> Self {
        Self(err.into())
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {
    init_logging()?;

    let state = AppState {
        client: reqwest::ClientBuilder::new()
            .user_agent("dewiki-votecheck")
            .build()?,
        query_cache: Arc::new(Mutex::new(HashMap::new())),
        check_sem: Arc::new(Semaphore::new(10)),
    };

    let index_handler = move || async {
        let index_html: &'static str = include_str!("index.html");
        Ok::<_, AppError>(Html(index_html))
    };
    let app = Router::new()
        .route("/", routing::get(index_handler))
        .route("/check", routing::get(check_page))
        .with_state(state)
        .layer(TraceLayer::new_for_http());

    let port = std::env::var("PORT")
        .unwrap_or_else(|_| "8000".to_string())
        .parse()?;

    #[cfg(debug_assertions)]
    let addr = SocketAddr::from(([127, 0, 0, 1], port));
    #[cfg(not(debug_assertions))]
    let addr = SocketAddr::from(([0, 0, 0, 0], port));
    tracing::debug!("listening on {}", addr);
    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();
    axum::serve(listener, app).await.unwrap();
    Ok(())
}
